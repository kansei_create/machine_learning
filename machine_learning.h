#include <vector>       // ヘッダファイルインクルード
#include <iostream>
#include <string>

#ifndef MACHINE_LEARNING_H
#define MACHINE_LEARNING_H

using namespace std;

class learn
{
  private:
  vector<vector<vector<double> > > weights;//重みを保存する
  vector<vector<double> > bias;//バイアスを保存する
  vector<vector<vector<double> > > d_weights;//各重みの微分後の値を保存する
  vector<vector<double> > d_bias;//各バイアスの微分後の値を保存する

  vector<vector<double> > hidden_layer;//各隠れ層の値を保存する
  vector<vector<double> > d_hidden_layer;//各隠れ層の微分後の値を保存する

  vector<double> result;//ニューラルネットワークの計算の結果を保存する

  vector<string> func_name;//活性化関数を保存する

  vector<double> input_data;//入力層の値を保存する

  vector<vector<double> > all_data;
  vector<vector<double> > all_answer;

  int layerNum;//レイヤーの数を保存

  string loss_func;


  vector<double> propagation_matrix_calculation(vector<double> front, vector<vector<double> > after);

  double activation(double x, string function_name, bool differential, int number);
  double d_loss(double x, double y, string loss_name);
  double soft_sign(double x);
  double d_soft_sign(double x);
  double sygmoid(double x);
  double d_sygmoid(double x);
  double softmax(double x ,int number);
  double d_softmax(double x, int number);
  double d_square_sum_error(double x , double y);
  double square_sum_error(vector<double> x, vector<double> y);

  void counterpropagation(vector<double> answer);
  vector<vector<vector<double> > > initialization_3(vector<vector<vector<double> > > a);
  vector<vector<double> > initialization_2(vector<vector<double> > a);
  void shhufle(vector<vector<double> > data, vector<vector<double> > answer);

  void update(vector<vector<double> > differential_bias, vector<vector<vector<double> > > differential_weights, int batch_size, double learnig_rate);

  void propagation(vector<double> data);//順伝播の計算を行う
  
  public:
  int get_first_weight(string file_name);

  void machine_learn(vector<vector<double> > answer, vector<vector<double> > data, int epoch,
		     int batch_size, double learning_rate , string loss);

  vector<double> predict(vector<double> data, bool show);

  bool write_data(string file_name);
  void debug();
  
  
};
#endif
